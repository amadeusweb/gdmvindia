<?php
include_once 'functions.php';

am_var('local', $local = $_SERVER['HTTP_HOST'] ==='localhost');

bootstrap(array(
	'name' => 'Global Digital Media Village',
	'safeName' => 'gdmvindia',
	'byline' => 'Profile of the Seasoned Filmmaker Muneer Ahamed',

	'version' => [ 'id' => '004', 'date' => '20 Feb 2022' ],

	'start_year' => '2018',
	'theme' => 'biz-land',
	'folder' => 'content/',

	'styles' => ['styles'],

	'email' => 'gdmv.muneer@gmail.com',
	'phone' => '919444380630',

	'social' => [
		['type' => 'linkedin', 'link' => 'https://www.linkedin.com/in/gdmv-muneer/'],
	],
	
	'url' => $local ? 'http://localhost/gdmvindia/' : '//gdmvindia.com/',

	'path' => SITEPATH,
	'no-local-stats' => true,
));

render();
?>
