<?php
load_amadeus_module('v2content');

read_contents(SITEPATH . '/data/content.tsv');

function before_file() {
	if (am_var('embed')) return;

	echo '<hr class="above-header-content" />' . am_var('nl');

	echo '<div id="content" class="container">';
}

function after_file() {
	if (am_var('embed')) return;

	echo '</div>';
}

?>
