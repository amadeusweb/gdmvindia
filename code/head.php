  <header class="masthead text-center text-white <?php echo cs_var('node'); ?>" style="<?php echo cs_var('node') == 'index' ? '' : 'background-image: url(../assets/'. cs_var('node') .'.jpg)'; ?>">
    <div class="masthead-content">
      <div class="container">
        <h1 class="masthead-heading mb-0"><?php content('Heading'); ?></h1>
        <?php if (cs_var('node') != 'resume') { ?><h2 class="masthead-subheading mb-0"><?php content('Sub Heading'); ?></h2><?php } ?>
        <a href="<?php content('CTA Link'); ?>" class="btn btn-primary btn-xl rounded-pill mt-5"><?php content('CTA'); ?></a>
      </div>
    </div>
  </header>

  <section>
    <div class="container">
