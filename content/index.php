  <section>
    <div class="container">
      <br>
      <div class="video-container"><iframe src="https://www.youtube.com/embed/NBVHUj9421o" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
      <br>
      <div class="video-container"><iframe src="https://www.youtube.com/embed/db8hd-SvLZg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
      <br>
      <div class="video-container"><iframe src="https://www.youtube.com/embed/sTmDR5nRYxg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
      <br>
      <div class="video-container"><iframe src="https://www.youtube.com/embed/Ytal3xgVAnI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
      <br>
      <div class="video-container"><iframe src="https://www.youtube.com/embed/VJcanfypAcg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
    </div>
  </section>

  <section>
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-6 order-lg-2">
          <div class="">
            <img class="img-fluid rounded-circle" src="assets/tree.jpg" alt="">
          </div>
        </div>
        <div class="col-lg-6 order-lg-1">
          <div class="p-5">
            <h2 class="display-4"><?php content('Section 1 Heading'); ?></h2>
            <p><?php content('Section 1 Para'); ?></p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-6">
          <div class="">
            <img class="img-fluid rounded-circle" src="assets/forest.jpg" alt="">
          </div>
        </div>
        <div class="col-lg-6">
          <div class="p-5">
            <h2 class="display-4"><?php content('Section 2 Heading'); ?></h2>
            <p><?php content('Section 2 Para'); ?></p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-6 order-lg-2">
          <div class="">
            <img class="img-fluid rounded-circle" src="assets/road.jpg" alt="">
          </div>
        </div>
        <div class="col-lg-6 order-lg-1">
          <div class="p-5">
            <h2 class="display-4"><?php content('Section 3 Heading'); ?></h2>
            <p><?php content('Section 3 Para'); ?></p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php if (has_content('Section 4 Heading')) { ?>
  <section>
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-6">
          <div class="p-5">
            <img class="img-fluid rounded-circle" src="assets/cinema.jpg" alt="">
          </div>
        </div>
        <div class="col-lg-6">
          <div class="p-5">
            <h2 class="display-4"><?php content('Section 4 Heading'); ?></h2>
            <p><?php content('Section 4 Para'); ?></p>
          </div>
        </div>
      </div>
    </div>
  </section><?php } ?>

